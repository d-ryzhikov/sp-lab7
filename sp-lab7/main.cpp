#include <Windows.h>

#define IDC_LISTBOX 101

#define IDC_EVENT   102
#define IDC_CS      103

HANDLE threads[2];

CRITICAL_SECTION critical_section;

HANDLE event;

bool terminate_threads = false;

LPCWSTR windowClassName = L"sp_lab7";

struct ThreadData 
{
    LPWSTR str;
    HWND hWnd;
};

DWORD WINAPI ThreadProc_event(LPVOID lpParameter);

DWORD WINAPI ThreadProc_cs(LPVOID lpParameter);

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hWnd;
    MSG Msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 1;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
    wc.lpszMenuName = nullptr;
    wc.lpszClassName = windowClassName;
    wc.hIconSm = nullptr;

    if (!RegisterClassEx(&wc))
    {
        MessageBox(nullptr, L"Couldn't register the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    hWnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        windowClassName,
        L"������������ ������ �7",
        WS_OVERLAPPEDWINDOW&~WS_MAXIMIZEBOX,
        CW_USEDEFAULT, CW_USEDEFAULT, 400, 400,
        nullptr, nullptr, hInstance,nullptr 
    );

    if (hWnd == nullptr)
    {
        MessageBox(nullptr, L"Couldn't create the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    while (GetMessage(&Msg, nullptr, 0, 0) > 0)
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    return Msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch (Msg)
    {
        case WM_CREATE: {
            CreateWindow(
                L"LISTBOX", nullptr,
                WS_CHILD | WS_VISIBLE | LBS_STANDARD | LBS_NODATA,
                1, 1, 250, 360,
                hWnd, reinterpret_cast<HMENU>(IDC_LISTBOX),
                GetModuleHandle(nullptr), nullptr
            );

            CreateWindow(
                L"BUTTON", L"Event",
                WS_VISIBLE | WS_CHILD | BS_RADIOBUTTON,
                255, 1, 60, 20,
                hWnd, reinterpret_cast<HMENU>(IDC_EVENT),
                GetModuleHandle(nullptr), nullptr
            );

            CreateWindow(
                L"BUTTON", L"CriticalSection",
                WS_VISIBLE | WS_CHILD | BS_RADIOBUTTON,
                255, 25, 120, 20,
                hWnd, reinterpret_cast<HMENU>(IDC_CS),
                GetModuleHandle(nullptr), nullptr
            );

            break;
        }
        case WM_COMMAND:
            if (HIWORD(wParam) == BN_CLICKED)
            {
                bool event_active = IsDlgButtonChecked(hWnd,
                    IDC_EVENT);
                bool cs_active = IsDlgButtonChecked(hWnd, IDC_CS);

                switch (LOWORD(wParam))
                {
                    case IDC_EVENT:
                    {
                        CheckRadioButton(hWnd, IDC_EVENT, IDC_CS,
                            IDC_EVENT);

                        if (cs_active)
                        {
                            EnterCriticalSection(&critical_section);
                            terminate_threads = true;
                            LeaveCriticalSection(&critical_section);
                            WaitForMultipleObjects(2, threads, TRUE, INFINITE);
                            DeleteCriticalSection(&critical_section);
                            terminate_threads = false;
                        }
                        if (!event_active)
                        {
                            event = CreateEvent(nullptr, TRUE, FALSE, nullptr);

                            ThreadData* data_1 = static_cast<ThreadData*>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(ThreadData))
                            );

                            ThreadData* data_2 = static_cast<ThreadData*>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(ThreadData))
                            );

                            data_1->hWnd = data_2->hWnd = hWnd;

                            data_1->str = static_cast<LPWSTR>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(L"First"))
                            );
                            lstrcpy(data_1->str, L"First");

                            data_2->str = static_cast<LPWSTR>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(L"Second"))
                            );
                            lstrcpy(data_2->str, L"Second");

                            threads[0] = CreateThread(nullptr, 0,
                                ThreadProc_event, data_1, 0, nullptr);
                            threads[1] = CreateThread(nullptr, 0,
                                ThreadProc_event, data_2, 0, nullptr);

                            SetEvent(event);
                        }
                        break;
                    }
                    case IDC_CS:
                    {
                        CheckRadioButton(hWnd, IDC_EVENT, IDC_CS, IDC_CS);

                        if (event_active)
                        {
                            ResetEvent(event);
                            terminate_threads = true;
                            SetEvent(event);
                            WaitForMultipleObjects(2, threads, TRUE, INFINITE);
                            CloseHandle(event);
                            terminate_threads = false;
                        }
                        if (!cs_active)
                        {
                            InitializeCriticalSection(&critical_section);

                            ThreadData* data_1 = static_cast<ThreadData*>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(ThreadData))
                            );

                            ThreadData* data_2 = static_cast<ThreadData*>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(ThreadData))
                            );

                            data_1->hWnd = data_2->hWnd = hWnd;

                            data_1->str = static_cast<LPWSTR>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(L"First"))
                            );
                            lstrcpy(data_1->str, L"First");

                            data_2->str = static_cast<LPWSTR>(
                                HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                    sizeof(L"Second"))
                            );
                            lstrcpy(data_2->str, L"Second");

                            threads[0] = CreateThread(nullptr, 0,
                                ThreadProc_cs, data_1, 0, nullptr);
                            threads[1] = CreateThread(nullptr, 0,
                                ThreadProc_cs, data_2, 0, nullptr);
                        }
                        break;
                    }
                }
            }
            break;
        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*>(lParam);
            mmi->ptMinTrackSize.x = mmi->ptMaxTrackSize.x = 400;
            mmi->ptMinTrackSize.y = mmi->ptMaxTrackSize.y = 400;
            break;
        }
        case WM_CLOSE:
            DestroyWindow(hWnd);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hWnd, Msg, wParam, lParam);
    }

    return 0;
}

DWORD WINAPI ThreadProc_event(LPVOID lpParameter)
{
    ThreadData* data = static_cast<ThreadData*>(lpParameter);

    while (true)
    {
        WaitForSingleObject(event, INFINITE);
        ResetEvent(event);

        if (terminate_threads)
        {
            SetEvent(event);
            break;
        }

        SendDlgItemMessage(data->hWnd, IDC_LISTBOX, LB_ADDSTRING, 0,
            reinterpret_cast<LPARAM>(data->str));

        SetEvent(event);
        Sleep(1000);
    }

    return 0;
}

DWORD WINAPI ThreadProc_cs(LPVOID lpParameter)
{
    ThreadData* data = static_cast<ThreadData*>(lpParameter);

    while (true)
    {
        EnterCriticalSection(&critical_section);

        if (terminate_threads)
        {
            LeaveCriticalSection(&critical_section);
            break;
        }

        SendDlgItemMessage(data->hWnd, IDC_LISTBOX, LB_ADDSTRING, 0,
            reinterpret_cast<LPARAM>(data->str));

        LeaveCriticalSection(&critical_section);
        Sleep(1000);
    }

    return 0;
}
